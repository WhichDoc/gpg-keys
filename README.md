- [plague-master.gpg](https://git.envs.net/WhichDoc/gpg-keys/src/branch/master/plague-master.gpg) is the PlagueOS official master key
- [plague-r2-signing-key.gpg](https://git.envs.net/WhichDoc/gpg-keys/src/branch/master/plague-r2-signing-key.gpg) is the key signed by `plague-master.gpg`, which is used to sign the release files

Master key fingerprint (also present in PlagueOS Matrix chat):
```
pub   rsa4096/0xE5BDCACE3D6DE768 2021-11-05 [SC]
Key fingerprint = E3FE 9E61 325B 9FCE 7FC2  34D1 E5BD CACE 3D6D E768
uid                   [ unknown] plagueOS Master Signing Key (Official Signing Key for WhichDoc Ops) [whichdoc@null.ch](mailto:whichdoc@null.ch)
```